//Dependencies
import styled from "styled-components";

const Form = styled.form`
  width: 90%;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 13px;
`;

export default Form;
